## Avaliação do php

Desenvolver lista de usuários de planos de saúde com os seguintes itens:

- Nome
- Idade
- Plano
- CPF
- Telefone (s)
- Quantidade de dependentes
- Mensalidade atual
- Apartamento (sim ou não)

Funcionalidades necessárias

- Cadastrar, editar e remover usuário do plano
- Adicionar mais de um telefone
- Selecionar entre os seguintes planos, Plano Básico, Plano médio ou Plano Master
- Calcular o valor da mensalidade atual com as seguintes regras:

#### Plano Básico

	0 até 30 anos = R$ 180
	30 até 50 anos = R$ 260
	50 anos ou + = R$ 440

	Dependentes não pagam

	Adiciona 10% se for apartamento


#### Plano Médio

	0 até 30 anos = R$ 200
	30 até 50 anos = R$ 320
	50 anos ou + = R$ 550

	R$ 20 cada dependente

	Se for apartamento e tiver dependente adicionar 15%
	Se for apartamento e não tiver dependente adicionar 10%



#### Plano Master 

	0 até 30 anos = R$ 250
	30 até 50 anos = R$ 350
	50 anos ou + = R$ 600

	aumenta 3% a cada dependente

	Se for apartamento aumenta R$ 50


### Regras

- A aplicação deverá ser desenvolvida na linguagem PHP sem uso de frameworks
- Deverá ser utilizado o banco de dados mysql
- Todo o código deverá ser commitado nesse repositório em um branch com o seu primeiro nome